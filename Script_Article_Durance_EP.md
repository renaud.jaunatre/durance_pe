---
title: ""
output: 
  html_document: 
    toc: yes
    keep_md: yes
editor_options: 
  chunk_output_type: console
---

# Script and analyses for the paper *Using priority effects for grassland restoration: Sequential sowing can promote subordinate species*

doi:10.1111/avsc.12748

## Preamble

### Packages

```r
library(Renaudpack2) #rar.rm #multivar.polyg #MultiDyn #label.corV2 
#barres.plot.beside #barres.plot
library(vegan)
library(lme4) # LMER 
library(knitr) # fonction kable
library(RVAideMemoire) # fonction plotresid pour glm
library(car) # fonction Anova pour glm
library(emmeans) 
library(multcomp) # fonction cld
library(ggplot2)
library(hrbrthemes)
```

### Tables


```r
Cover <- read.csv("https://forgemia.inra.fr/renaud.jaunatre/durance_pe/-/raw/main/Data_Durance_EP.csv", 
                  header=T, sep=";",stringsAsFactors = T)

Cover$Year <- as.factor(Cover$Year)
Cover$Quadrat <- as.factor(Cover$Quadrat)
Cover$Compo <- factor(as.factor(Cover$Compo),levels=c("12","1","2","T"))

Sp_sem <- c("Festuca_cinerea","Onobrychis_viciifolia","Plantago_lanceolata",
            "Anthyllis_vulneraria","Bromopsis_erecta", "Plantago_media") 

Sp_sem%in%names(Cover) # to check if the columns names are OK
```

```
## [1] TRUE TRUE TRUE TRUE TRUE TRUE
```


## Figure 2 - Multivariate analyses on composition 



```r
coul_NMDS <- c("#114958","#F59036",  "#7AC9A3", "#96A393")
#definition of colors

releves <- rar.rm(Cover[,6:ncol(Cover)], n=1)
#removal of species that don't occur at least one time (::Renaudpack2)

NMDS_EP<-metaMDS(releves, )
```

```
## Square root transformation
## Wisconsin double standardization
## Run 0 stress 0.2415854 
## Run 1 stress 0.2415368 
## ... New best solution
## ... Procrustes: rmse 0.01261153  max resid 0.1234158 
## Run 2 stress 0.2441498 
## Run 3 stress 0.2426448 
## Run 4 stress 0.242724 
## Run 5 stress 0.2581887 
## Run 6 stress 0.2427104 
## Run 7 stress 0.254375 
## Run 8 stress 0.2466816 
## Run 9 stress 0.2418423 
## ... Procrustes: rmse 0.01377982  max resid 0.1244353 
## Run 10 stress 0.2460674 
## Run 11 stress 0.2493424 
## Run 12 stress 0.2541544 
## Run 13 stress 0.2434082 
## Run 14 stress 0.2611047 
## Run 15 stress 0.2470236 
## Run 16 stress 0.2500209 
## Run 17 stress 0.2525969 
## Run 18 stress 0.2503416 
## Run 19 stress 0.2443716 
## Run 20 stress 0.2443777 
## *** Best solution was not repeated -- monoMDS stopping criteria:
##      5: no. of iterations >= maxit
##     15: stress ratio > sratmax
```

```r
NMDS_EP$stress
```

```
## [1] 0.2415368
```

```r
plot(NMDS_EP$points,col="gray80", ylab="NMDS axis 2", 
     xlab = "NMDS axis 1", type="n")
# the frame
multivar.polyg(ANAcoo =  NMDS_EP$points, FAC = Cover$Compo,
               col_dot = coul_NMDS, col_bord = "white", new = FALSE, lab = FALSE)
# the points by treatments (::Renaudpack2)
MultiDyn(MultCOO = NMDS_EP$points,FACTime = Cover$Year,
         FACT2 = Cover$Compo, ARR = T, COL = coul_NMDS, new = FALSE)
# the trajectories by treatments (::Renaudpack2)
multivar.polyg(ANAcoo =  NMDS_EP$points[Cover$Year == 2021,], 
               FAC = Cover$Compo[Cover$Year == 2021],
               col_dot = coul_NMDS, col_bord = coul_NMDS, 
               new = FALSE, pch=16, lab = FALSE)
# the polygons by treatments (::Renaudpack2)
label.corV2(COO_ESP = NMDS_EP$species, COO_REL = NMDS_EP$points, 
            RELEVES = releves, METHOD = "N_base_P",N = 15,ADD = T,font=3,  
            COEF = 1.3)
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-3-1.png)<!-- -->

```r
# the most correlated species (::Renaudpack2)
```


```r
adonis2(releves ~ Cover$Compo) #adonis test
betadisp_S <- betadisper(d = vegdist(releves), group = Cover$Compo ) 
## Calculate multivariate dispersions
anova(betadisp_S) ## Perform test
permutest(betadisp_S, pairwise = TRUE, permutations = 999) 
## Permutation test for F
(modS.HSD <- TukeyHSD(betadisp_S)) ## Tukey's Honest Significant Differences
```

## Figure 3 - stacked plot of the species covers

Data preparation


```r
Species <- factor(rep(c("Bromopsis_erecta","Festuca_cinerea",
                        "Plantago_lanceolata","Anthyllis_vulneraria",
                        "Onobrychis_viciifolia","unsown"),4),
                  levels=c("Bromopsis_erecta", "Festuca_cinerea",   
                           "Plantago_lanceolata", "Anthyllis_vulneraria", 
                           "Onobrychis_viciifolia", "unsown"))
Composition <- factor(rep(c("1","2","12","T"),each=6),
                      levels=c("12","2","1", "T"))
Mean_cover <- NULL
for(i in 1:length(Species))
{
  Cov_i <- Cover[Cover$Compo==Composition[i] & Cover$Year==2021,
                 names(Cover)==Species[i]]
  Mean_cover[i] <- mean(Cov_i,na.rm=T)
}
MeanCov2021 <- data.frame(Composition,Species,Mean_cover)
```

The plot


```r
xscale2<-scale_x_discrete(labels=c("D+S" ,"D1st", "S1st", "Control")) 
col_stacked <- c("#2738B3","#7E8CF7","#2FFAF3","#128510","#8CFF8A","grey")
ylabobj<-ylab("Composition of the different treatments (% cover)")

ggplot()+
  geom_bar(data=MeanCov2021,aes(x=Composition, y=Mean_cover, fill=Species),
           stat="identity",position="stack")+ 
    xscale2+
  ylabobj+
  scale_fill_manual(values = col_stacked)+
  scale_y_continuous(breaks=seq(0,100.00,10))+
    ggtitle("") +
    theme_ipsum() +
    xlab("") + 
    ylab("Species cover in 2021")+
  theme(plot.title = element_blank(),
        axis.title.x = element_text(size=14, colour="black", face = 1),
        axis.title.y = element_text(size=14, colour="black"),
        axis.text.x=element_text(size=14, colour="black" ), 
        axis.text.y=element_text(size=10, colour="black"),
        axis.line.x=element_line(colour="black",size=0.8),
        axis.line.y=element_line(colour="black",size=0.8),
        axis.ticks.x = ,
        axis.ticks.y=element_line(colour="black",size=0.8),
        panel.background=element_blank(),
        panel.grid.major = element_blank(),
        panel.grid.minor=element_blank(),
        legend.position = )
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-6-1.png)<!-- -->

## Figure 4 - Sown species cover in the different treatments

Data preparation


```r
Data_2021 <- Cover[Cover$Year==2021,]
Data_2021wc <- droplevels(Data_2021[Data_2021$Compo!="T",]) #removing control

Facteur_SownSpwc <- factor(rep(c("Anthyllis_vulneraria","Bromopsis_erecta","Onobrychis_viciifolia","Plantago_lanceolata","Festuca_cinerea"),each=40), levels=c("Bromopsis_erecta","Anthyllis_vulneraria","Festuca_cinerea","Onobrychis_viciifolia","Plantago_lanceolata"))

Variable_SownCover <- c(Data_2021$Anthyllis_vulneraria, 
                        Data_2021$Bromopsis_erecta, 
                        Data_2021$Onobrychis_viciifolia ,
                        Data_2021$Plantago_lanceolata,
                        Data_2021$Festuca_cinerea)

Compo2wc <-factor(Data_2021$Compo,levels=c("12","2","1"))
Compo3wc <- factor(rep(Compo2wc,5))
```

The GLMs and posthoc for each species


```r
Data_2021wc$Bromopsis_erecta <- Data_2021wc$Bromopsis_erecta/100
Data_2021wc$Anthyllis_vulneraria <- Data_2021wc$Anthyllis_vulneraria/100
Data_2021wc$Onobrychis_viciifolia <- Data_2021wc$Onobrychis_viciifolia/100
Data_2021wc$Festuca_cinerea <- Data_2021wc$Festuca_cinerea/100
Data_2021wc$Plantago_lanceolata <- Data_2021wc$Plantago_lanceolata/100

brome <-glm(Bromopsis_erecta ~ Compo, family=quasibinomial, data=Data_2021wc)
plotresid(brome)
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-8-1.png)<!-- -->

```r
summary(brome)
```

```
## 
## Call:
## glm(formula = Bromopsis_erecta ~ Compo, family = quasibinomial, 
##     data = Data_2021wc)
## 
## Deviance Residuals: 
##      Min        1Q    Median        3Q       Max  
## -0.96954  -0.18126   0.05142   0.09610   0.65836  
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)  -0.8142     0.1912  -4.259 0.000222 ***
## Compo1       -2.0012     0.4270  -4.686 7.08e-05 ***
## Compo2        0.3034     0.2640   1.149 0.260659    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## (Dispersion parameter for quasibinomial family taken to be 0.0777441)
## 
##     Null deviance: 6.0342  on 29  degrees of freedom
## Residual deviance: 2.4585  on 27  degrees of freedom
## AIC: NA
## 
## Number of Fisher Scoring iterations: 5
```

```r
anova(brome,test="F")
```

```
## Analysis of Deviance Table
## 
## Model: quasibinomial, link: logit
## 
## Response: Bromopsis_erecta
## 
## Terms added sequentially (first to last)
## 
## 
##       Df Deviance Resid. Df Resid. Dev      F    Pr(>F)    
## NULL                     29     6.0342                     
## Compo  2   3.5757        27     2.4585 22.997 1.476e-06 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

```r
emmeans(brome, pairwise ~ Compo, adjust="tukey",alpha = .05) # POST-HOC GLM
```

```
## $emmeans
##  Compo emmean    SE  df asymp.LCL asymp.UCL
##  12    -0.814 0.191 Inf    -1.189    -0.440
##  1     -2.815 0.382 Inf    -3.564    -2.067
##  2     -0.511 0.182 Inf    -0.868    -0.154
## 
## Results are given on the logit (not the response) scale. 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast         estimate    SE  df z.ratio p.value
##  Compo12 - Compo1    2.001 0.427 Inf   4.686  <.0001
##  Compo12 - Compo2   -0.303 0.264 Inf  -1.149  0.4839
##  Compo1 - Compo2    -2.305 0.423 Inf  -5.447  <.0001
## 
## Results are given on the log odds ratio (not the response) scale. 
## P value adjustment: tukey method for comparing a family of 3 estimates
```

```r
fetuque <-glm(Festuca_cinerea ~ Compo, family=quasibinomial, data=Data_2021wc)
plotresid(fetuque)
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-8-2.png)<!-- -->

```r
summary(fetuque)
```

```
## 
## Call:
## glm(formula = Festuca_cinerea ~ Compo, family = quasibinomial, 
##     data = Data_2021wc)
## 
## Deviance Residuals: 
##      Min        1Q    Median        3Q       Max  
## -0.63890  -0.18683   0.01345   0.16088   0.63358  
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)  -1.8029     0.2602  -6.930  1.9e-07 ***
## Compo1        0.4445     0.3438   1.293  0.20708    
## Compo2       -1.9732     0.6657  -2.964  0.00627 ** 
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## (Dispersion parameter for quasibinomial family taken to be 0.0822259)
## 
##     Null deviance: 4.4958  on 29  degrees of freedom
## Residual deviance: 2.5777  on 27  degrees of freedom
## AIC: NA
## 
## Number of Fisher Scoring iterations: 6
```

```r
anova(fetuque,test="F")
```

```
## Analysis of Deviance Table
## 
## Model: quasibinomial, link: logit
## 
## Response: Festuca_cinerea
## 
## Terms added sequentially (first to last)
## 
## 
##       Df Deviance Resid. Df Resid. Dev      F    Pr(>F)    
## NULL                     29     4.4958                     
## Compo  2   1.9182        27     2.5777 11.664 0.0002234 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

```r
emmeans(fetuque, pairwise ~ Compo, adjust="tukey",alpha = .05) 
```

```
## $emmeans
##  Compo emmean    SE  df asymp.LCL asymp.UCL
##  12     -1.80 0.260 Inf     -2.31    -1.293
##  1      -1.36 0.225 Inf     -1.80    -0.918
##  2      -3.78 0.613 Inf     -4.98    -2.575
## 
## Results are given on the logit (not the response) scale. 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast         estimate    SE  df z.ratio p.value
##  Compo12 - Compo1   -0.444 0.344 Inf  -1.293  0.3993
##  Compo12 - Compo2    1.973 0.666 Inf   2.964  0.0085
##  Compo1 - Compo2     2.418 0.653 Inf   3.704  0.0006
## 
## Results are given on the log odds ratio (not the response) scale. 
## P value adjustment: tukey method for comparing a family of 3 estimates
```

```r
onobrychis <-glm(Onobrychis_viciifolia ~ Compo, family=quasibinomial,
                 data=Data_2021wc)
plotresid(onobrychis)
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-8-3.png)<!-- -->

```r
summary(onobrychis)
```

```
## 
## Call:
## glm(formula = Onobrychis_viciifolia ~ Compo, family = quasibinomial, 
##     data = Data_2021wc)
## 
## Deviance Residuals: 
##      Min        1Q    Median        3Q       Max  
## -0.36370  -0.19404  -0.05935   0.10723   0.50826  
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)  -2.6827     0.2871  -9.345 5.95e-10 ***
## Compo1        1.0829     0.3431   3.156  0.00391 ** 
## Compo2       -0.7934     0.5021  -1.580  0.12570    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## (Dispersion parameter for quasibinomial family taken to be 0.0493677)
## 
##     Null deviance: 2.5006  on 29  degrees of freedom
## Residual deviance: 1.2267  on 27  degrees of freedom
## AIC: NA
## 
## Number of Fisher Scoring iterations: 6
```

```r
anova(onobrychis,test="F")
```

```
## Analysis of Deviance Table
## 
## Model: quasibinomial, link: logit
## 
## Response: Onobrychis_viciifolia
## 
## Terms added sequentially (first to last)
## 
## 
##       Df Deviance Resid. Df Resid. Dev      F    Pr(>F)    
## NULL                     29     2.5006                     
## Compo  2   1.2739        27     1.2267 12.902 0.0001168 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

```r
emmeans(onobrychis, pairwise ~ Compo, adjust="tukey",alpha = .05) 
```

```
## $emmeans
##  Compo emmean    SE  df asymp.LCL asymp.UCL
##  12     -2.68 0.287 Inf     -3.25     -2.12
##  1      -1.60 0.188 Inf     -1.97     -1.23
##  2      -3.48 0.412 Inf     -4.28     -2.67
## 
## Results are given on the logit (not the response) scale. 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast         estimate    SE  df z.ratio p.value
##  Compo12 - Compo1   -1.083 0.343 Inf  -3.156  0.0046
##  Compo12 - Compo2    0.793 0.502 Inf   1.580  0.2542
##  Compo1 - Compo2     1.876 0.453 Inf   4.144  0.0001
## 
## Results are given on the log odds ratio (not the response) scale. 
## P value adjustment: tukey method for comparing a family of 3 estimates
```

```r
plantain <-glm(Plantago_lanceolata ~ Compo, family=quasibinomial,
               data=Data_2021wc)
plotresid(plantain)
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-8-4.png)<!-- -->

```r
summary(plantain)
```

```
## 
## Call:
## glm(formula = Plantago_lanceolata ~ Compo, family = quasibinomial, 
##     data = Data_2021wc)
## 
## Deviance Residuals: 
##      Min        1Q    Median        3Q       Max  
## -0.32871  -0.12020  -0.07698   0.04800   0.31933  
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)  -4.2546     0.4810  -8.845 1.84e-09 ***
## Compo1        1.6679     0.5296   3.150  0.00397 ** 
## Compo2       -1.1009     0.9561  -1.151  0.25966    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## (Dispersion parameter for quasibinomial family taken to be 0.03194005)
## 
##     Null deviance: 1.61466  on 29  degrees of freedom
## Residual deviance: 0.76439  on 27  degrees of freedom
## AIC: NA
## 
## Number of Fisher Scoring iterations: 8
```

```r
anova(plantain,test="F")
```

```
## Analysis of Deviance Table
## 
## Model: quasibinomial, link: logit
## 
## Response: Plantago_lanceolata
## 
## Terms added sequentially (first to last)
## 
## 
##       Df Deviance Resid. Df Resid. Dev     F    Pr(>F)    
## NULL                     29    1.61466                    
## Compo  2  0.85028        27    0.76439 13.31 9.493e-05 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

```r
emmeans(plantain, pairwise ~ Compo, adjust="tukey",alpha = .05) 
```

```
## $emmeans
##  Compo emmean    SE  df asymp.LCL asymp.UCL
##  12     -4.25 0.481 Inf     -5.20     -3.31
##  1      -2.59 0.222 Inf     -3.02     -2.15
##  2      -5.36 0.826 Inf     -6.98     -3.74
## 
## Results are given on the logit (not the response) scale. 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast         estimate    SE  df z.ratio p.value
##  Compo12 - Compo1    -1.67 0.530 Inf  -3.150  0.0047
##  Compo12 - Compo2     1.10 0.956 Inf   1.151  0.4824
##  Compo1 - Compo2      2.77 0.855 Inf   3.237  0.0035
## 
## Results are given on the log odds ratio (not the response) scale. 
## P value adjustment: tukey method for comparing a family of 3 estimates
```

```r
anthyllis <-glm(Anthyllis_vulneraria ~ Compo, family=quasibinomial, 
                data=Data_2021wc)
plotresid(anthyllis)
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-8-5.png)<!-- -->

```r
summary(anthyllis)
```

```
## 
## Call:
## glm(formula = Anthyllis_vulneraria ~ Compo, family = quasibinomial, 
##     data = Data_2021wc)
## 
## Deviance Residuals: 
##      Min        1Q    Median        3Q       Max  
## -0.22502  -0.13196  -0.03354   0.09835   0.27761  
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)  -3.6636     0.2967 -12.346 1.29e-12 ***
## Compo1        0.6541     0.3687   1.774   0.0874 .  
## Compo2        0.4044     0.3851   1.050   0.3029    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## (Dispersion parameter for quasibinomial family taken to be 0.02146335)
## 
##     Null deviance: 0.65620  on 29  degrees of freedom
## Residual deviance: 0.58519  on 27  degrees of freedom
## AIC: NA
## 
## Number of Fisher Scoring iterations: 6
```

```r
anova(anthyllis,test="F")
```

```
## Analysis of Deviance Table
## 
## Model: quasibinomial, link: logit
## 
## Response: Anthyllis_vulneraria
## 
## Terms added sequentially (first to last)
## 
## 
##       Df Deviance Resid. Df Resid. Dev      F Pr(>F)
## NULL                     29    0.65620              
## Compo  2  0.07101        27    0.58519 1.6542   0.21
```

```r
emmeans(anthyllis, pairwise ~ Compo, adjust="tukey",alpha = .05) 
```

```
## $emmeans
##  Compo emmean    SE  df asymp.LCL asymp.UCL
##  12     -3.66 0.297 Inf     -4.25     -3.08
##  1      -3.01 0.219 Inf     -3.44     -2.58
##  2      -3.26 0.245 Inf     -3.74     -2.78
## 
## Results are given on the logit (not the response) scale. 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast         estimate    SE  df z.ratio p.value
##  Compo12 - Compo1   -0.654 0.369 Inf  -1.774  0.1784
##  Compo12 - Compo2   -0.404 0.385 Inf  -1.050  0.5451
##  Compo1 - Compo2     0.250 0.329 Inf   0.759  0.7281
## 
## Results are given on the log odds ratio (not the response) scale. 
## P value adjustment: tukey method for comparing a family of 3 estimates
```

The plot


```r
colfig5 <- c("#114958","#7AC9A3","#F59036")
barres.plot.beside(VARI = Variable_SownCover, FAC1 = Facteur_SownSpwc , 
                   FAC2 = Compo3wc, POSI = "bottom", ylim = c(0,50), las=1, 
                   ylab=" Sown species cover in 2021 (%)", cex.x = 1, font.x=3,
                   font.lab=2, cex.lab=1.2, col=colfig5, border=colfig5, 
                   lettres=c( "a", "a", "b", "a", "a", "a", "a", "b","a",
                              "a", "a", "b",  "a", "a", "b" ))
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-9-1.png)<!-- -->


## Indices calculation

We used two indices to assess historical contingency processes. The first one, priority index (PI - equation 1), measures priority effect sensu stricto (i.e. the effect of early species on late arriving species) and the second one, earlyness index (EI - equation 2) measures the benefit of arriving early in the community compared to synchronous arrival with other species. These indices behave as additive neighbour‐effect intensity index (Diaz-Serra et al., 2017): they are standardized, symmetric (additive symmetry), and bounded between −1 (competitive exclusion of late‐arriving species (PI) or by species sown synshronously (EI)) and +2 (obligate facilitation by early‐arriving species (PI) or by species sown synchronously) (Delory et al., 2019 / Diaz-Serra et al., 2017). Used together, these two indices provide information about the symmetry in biotic interactions by sowing time as Cleland et al. (2015), where PI is very close to their *secondary effect* and EI to their *priority effect*.

$$(1): PI_{n,i}=2\frac{Y^{Late}_{n,i} – \overline{Y^{Sync}_{n}}}{\overline{Y^{Sync}_{n}} + \left|Y^{Late}_{n,i} – \overline{Y^{Sync}_{n}}\right|}$$
$$(2): EI_{n,i}=2\frac{Y^{Early}_{n,i} – \overline{Y^{Sync}_{n}}}{\overline{Y^{Sync}_{n}} + \left|Y^{Early}_{n,i} – \overline{Y^{Sync}_{n}}\right|}$$
where $Y^{Late}_{n,i}$ and $Y^{Early}_{n,i}$ are the cover of one species sown respectively late and early in the plot $i$, $n$ years after being sown and $\overline{Y^{Sync}_{n}}$ the average cover of that same species when sown in synchrony.

As the time between sowing event is one year we used plant covers of the same age and not necessarily from the same monitored year. This could introduce a bias if rainfall, temperature or any environmental variable differed between years. This bias appeared however less important than the one that would be introduced by using plant covers of different ages.

Calculus of indivicual ages

Compo 1 = S1st ; Compo 2 : D1st ; Compo 12 : Sync

* if species are dominant (*Bromopsis erecta*, *Anthyllis vulneraria*, *Plantago media*)
  * si semis 1 
    * 0 an en 2019
    * 1 an en 2020
    * 2 ans en 2021
  * si semis 2 ou 12
    * 1 an en 2019
    * 2 ans en 2020
    * 3 ans en 2021
* if species are subordinates (*Festuca cinerea*, *Onobrychis viciifolia*, *Plantago lanceolata*)
  * si semis 2
    * 0 an en 2019
    * 1 an en 2020
    * 2 ans en 2021
  * si semis 1 ou 12
    * 1 an en 2019
    * 2 ans en 2020
    * 3 ans en 2021

## Figure 5 - indices

Calculus by summing the 3 species:


```r
Cover_dom <- Cover$Bromopsis_erecta+Cover$Anthyllis_vulneraria
Cover_sub <- Cover$Festuca_cinerea+Cover$Onobrychis_viciifolia+Cover$Plantago_lanceolata

Y_Late_1_i_dom <- NULL
Y_Early_1_i_dom <- NULL
PI_1_dom <- NULL
EI_1_dom <- NULL
Y_Late_2_i_dom <- NULL
Y_Early_2_i_dom <- NULL
PI_2_dom <- NULL
EI_2_dom <- NULL
Y_Late_1_i_sub <- NULL
Y_Early_1_i_sub <- NULL
PI_1_sub <- NULL
EI_1_sub <- NULL
Y_Late_2_i_sub <- NULL
Y_Early_2_i_sub <- NULL
PI_2_sub <- NULL
EI_2_sub <- NULL

for(i in 1:10)
  {
    #Calculus for dom
    
    #Calculus for year n = 1
    Y_Late_1_i_dom[i] <- Cover_dom[Cover$Year==2020 & Cover$Compo=="1"][i]
    Y_Early_1_i_dom[i] <- Cover_dom[Cover$Year==2019 & Cover$Compo=="2"][i]
    Y_Sync_1_dom <- mean(Cover_dom[Cover$Year==2019 & Cover$Compo=="12"],
                         na.rm = T)
    PI_1_dom[i] <- 2*(Y_Late_1_i_dom[i]-Y_Sync_1_dom)/(Y_Sync_1_dom+abs(Y_Late_1_i_dom[i]-Y_Sync_1_dom))
    EI_1_dom[i] <- 2*(Y_Early_1_i_dom[i]-Y_Sync_1_dom)/(Y_Sync_1_dom+abs(Y_Early_1_i_dom[i]-Y_Sync_1_dom))
    
    #Calculus for year n = 2
    Y_Late_2_i_dom[i] <- Cover_dom[Cover$Year==2021 & Cover$Compo=="1"][i]
    Y_Early_2_i_dom[i] <- Cover_dom[Cover$Year==2020 & Cover$Compo=="2"][i]
    Y_Sync_2_dom <- mean(Cover_dom[Cover$Year==2020 & Cover$Compo=="12"],
                         na.rm = T)
    PI_2_dom[i] <- 2*(Y_Late_2_i_dom[i]-Y_Sync_2_dom)/(Y_Sync_2_dom+abs(Y_Late_2_i_dom[i]-Y_Sync_2_dom))
    EI_2_dom[i] <- 2*(Y_Early_2_i_dom[i]-Y_Sync_2_dom)/(Y_Sync_2_dom+abs(Y_Early_2_i_dom[i]-Y_Sync_2_dom))
    
    # Calculus for sub :
    
    #Calculus for year n = 1
    Y_Late_1_i_sub[i] <- Cover_sub[Cover$Year==2020 & Cover$Compo=="2"][i]
    Y_Early_1_i_sub[i] <- Cover_sub[Cover$Year==2019 & Cover$Compo=="1"][i]
    Y_Sync_1_sub <- mean(Cover_sub[Cover$Year==2019 & Cover$Compo=="12"],
                         na.rm = T)
    PI_1_sub[i] <- 2*(Y_Late_1_i_sub[i]-Y_Sync_1_sub)/(Y_Sync_1_sub+abs(Y_Late_1_i_sub[i]-Y_Sync_1_sub))
    EI_1_sub[i] <- 2*(Y_Early_1_i_sub[i]-Y_Sync_1_sub)/(Y_Sync_1_sub+abs(Y_Early_1_i_sub[i]-Y_Sync_1_sub))
    
    #Calculus for year n = 2
    Y_Late_2_i_sub[i] <- Cover_sub[Cover$Year==2021 & Cover$Compo=="2"][i]
    Y_Early_2_i_sub[i] <- Cover_sub[Cover$Year==2020 & Cover$Compo=="1"][i]
    Y_Sync_2_sub <- mean(Cover_sub[Cover$Year==2020 & Cover$Compo=="12"],
                         na.rm = T)
    PI_2_sub[i] <- 2*(Y_Late_2_i_sub[i]-Y_Sync_2_sub)/(Y_Sync_2_sub+abs(Y_Late_2_i_sub[i]-Y_Sync_2_sub))
    EI_2_sub[i] <- 2*(Y_Early_2_i_sub[i]-Y_Sync_2_sub)/(Y_Sync_2_sub+abs(Y_Early_2_i_sub[i]-Y_Sync_2_sub))
  }

#Formatting for stat analyses and plot drawing
indicePI_1_SD <- c(PI_1_dom,PI_1_sub)
indicePI_2_SD <- c(PI_2_dom,PI_2_sub)
indiceEI_1_SD <- c(EI_1_dom,EI_1_sub)
indiceEI_2_SD <- c(EI_2_dom,EI_2_sub)

Facteur_indices_SD <- factor(rep(c("Dominant","Subordinate"),each=10))   

coul_IPSD <- c("gray25","gray75")
```

The stats


```r
Tab_indices <- data.frame(indicePI_2_SD, indiceEI_2_SD, Facteur_indices_SD)
```

* PI GLM


```r
indicePI_2_SD_t <- (indicePI_2_SD+1)/3
plot(indicePI_2_SD_t, indicePI_2_SD)
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-12-1.png)<!-- -->

```r
# transformation to be between 0 and 1
# check that it does not change the distribution structure
glm_t2 <- glm(indicePI_2_SD_t ~ Facteur_indices_SD , family=Gamma(link="log"), 
              data=Tab_indices)
plotresid(glm_t2)
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-12-2.png)<!-- -->

```r
summary(glm_t2)
```

```
## 
## Call:
## glm(formula = indicePI_2_SD_t ~ Facteur_indices_SD, family = Gamma(link = "log"), 
##     data = Tab_indices)
## 
## Deviance Residuals: 
##     Min       1Q   Median       3Q      Max  
## -2.0911  -0.6042  -0.2678   0.1627   2.4419  
## 
## Coefficients:
##                               Estimate Std. Error t value Pr(>|t|)    
## (Intercept)                    -2.7593     0.3939  -7.005 1.54e-06 ***
## Facteur_indices_SDSubordinate   0.2364     0.5570   0.424    0.676    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## (Dispersion parameter for Gamma family taken to be 1.551389)
## 
##     Null deviance: 22.901  on 19  degrees of freedom
## Residual deviance: 22.622  on 18  degrees of freedom
## AIC: -59.399
## 
## Number of Fisher Scoring iterations: 7
```

```r
anova(glm_t2,test="F")
```

```
## Analysis of Deviance Table
## 
## Model: Gamma, link: log
## 
## Response: indicePI_2_SD_t
## 
## Terms added sequentially (first to last)
## 
## 
##                    Df Deviance Resid. Df Resid. Dev      F Pr(>F)
## NULL                                  19     22.901              
## Facteur_indices_SD  1  0.27883        18     22.622 0.1797 0.6766
```

* EI GLM


```r
indiceEI_2_SD_t <- (indiceEI_2_SD+1)/3
plot(indiceEI_2_SD_t, indiceEI_2_SD)
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-13-1.png)<!-- -->

```r
# transformation to be between 0 and 1
# check that it does not change the distribution structure
glm_tEI <- glm(indiceEI_2_SD_t ~ Facteur_indices_SD , family=gaussian , 
               data=Tab_indices)
plotresid(glm_tEI)
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-13-2.png)<!-- -->

```r
summary(glm_tEI)
```

```
## 
## Call:
## glm(formula = indiceEI_2_SD_t ~ Facteur_indices_SD, family = gaussian, 
##     data = Tab_indices)
## 
## Deviance Residuals: 
##      Min        1Q    Median        3Q       Max  
## -0.33443  -0.11914   0.02927   0.11466   0.28019  
## 
## Coefficients:
##                               Estimate Std. Error t value Pr(>|t|)    
## (Intercept)                    0.33443    0.05911   5.657 2.29e-05 ***
## Facteur_indices_SDSubordinate  0.33610    0.08360   4.021 0.000802 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## (Dispersion parameter for gaussian family taken to be 0.03494227)
## 
##     Null deviance: 1.19379  on 19  degrees of freedom
## Residual deviance: 0.62896  on 18  degrees of freedom
## AIC: -6.4308
## 
## Number of Fisher Scoring iterations: 2
```

```r
Anova(glm_tEI,test="F")
```

```
## Analysis of Deviance Table (Type II tests)
## 
## Response: indiceEI_2_SD_t
## Error estimate based on Pearson residuals 
## 
##                     Sum Sq Df F value    Pr(>F)    
## Facteur_indices_SD 0.56483  1  16.165 0.0008023 ***
## Residuals          0.62896 18                      
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

The figure


```r
par(mfrow=c(1,2))
barres.plot(variable = indicePI_2_SD,Facteur = Facteur_indices_SD,
            ylim = c(-1.5,1.5), col = coul_IPSD, border= coul_IPSD, 
            ylab="PI (covers) n = 2 years", las=1, ecart = "IC")

barres.plot(variable = indiceEI_2_SD,Facteur = Facteur_indices_SD,
            ylim = c(-1.5,1.5), col = coul_IPSD, border= coul_IPSD, 
            ylab="EI (covers) n = 2 years", las=1, ecart = "IC")
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-14-1.png)<!-- -->

```r
par(mfrow=c(1,1))
```

Idem for all the sown species


```r
Y_Late_1_i <- NULL
Y_Early_1_i <- NULL
Y_Sync_1 <- NULL
Y_Late_2_i <- NULL
Y_Early_2_i <- NULL
Y_Sync_2 <- NULL

PI_1 <-NULL
EI_1 <-NULL
PI_2 <-NULL
EI_2 <-NULL

Tab_calculs <- data.frame(Num = c(1:10))

for(j in 1:length(Sp_sem))
{
  for(i in 1:10)
  {
    #Composition attribution
    CompoLate <- ifelse(Sp_sem[j]%in%c("Festuca_cinerea","Onobrychis_viciifolia","Plantago_lanceolata"),"2","1")
    CompoEarly <- ifelse(Sp_sem[j]%in%c("Festuca_cinerea","Onobrychis_viciifolia","Plantago_lanceolata"),"1","2")
    
    #Calculus for year n = 1
    Y_Late_1_i[i] <- Cover[Cover$Year==2020 & Cover$Compo==CompoLate,
                               names(Cover)==Sp_sem[j]][i]
    Y_Early_1_i[i] <- Cover[Cover$Year==2019 & Cover$Compo==CompoEarly,
                               names(Cover)==Sp_sem[j]][i]
    Y_Sync_1 <- mean(Cover[Cover$Year==2019 & Cover$Compo=="12",
                               names(Cover)==Sp_sem[j]],na.rm = T)
    PI_1[i] <- 2*(Y_Late_1_i[i]-Y_Sync_1)/(Y_Sync_1+abs(Y_Late_1_i[i]-Y_Sync_1))
    EI_1[i] <- 2*(Y_Early_1_i[i]-Y_Sync_1)/(Y_Sync_1+abs(Y_Early_1_i[i]-Y_Sync_1))
    
    #Calculus for year n = 2
    Y_Late_2_i[i] <- Cover[Cover$Year==2021 & Cover$Compo==CompoLate,
                               names(Cover)==Sp_sem[j]][i]
    Y_Early_2_i[i] <- Cover[Cover$Year==2020 & Cover$Compo==CompoEarly,
                               names(Cover)==Sp_sem[j]][i]
    Y_Sync_2 <- mean(Cover[Cover$Year==2020 & Cover$Compo=="12",
                               names(Cover)==Sp_sem[j]],na.rm = T)
    PI_2[i] <- 2*(Y_Late_2_i[i]-Y_Sync_2)/(Y_Sync_2+abs(Y_Late_2_i[i]-Y_Sync_2))
    EI_2[i] <- 2*(Y_Early_2_i[i]-Y_Sync_2)/(Y_Sync_2+abs(Y_Early_2_i[i]-Y_Sync_2))
  }
    
  
  Tab_calculs_tmp <- data.frame(Y_Late_1_i, Y_Early_1_i, Y_Sync_1, PI_1, EI_1,
                                Y_Late_2_i, Y_Early_2_i, Y_Sync_2, PI_2, EI_2)
  names(Tab_calculs_tmp) <- paste(names(Tab_calculs_tmp),Sp_sem[j],sep="_")
  #Concatenation of calculus for species j
  Tab_calculs <- data.frame(Tab_calculs,Tab_calculs_tmp)
  #Concatenation of calculus for all species
}

#Formatting for stat analyses and plot drawing
indicePI_1 <- c(Tab_calculs$PI_1_Anthyllis_vulneraria,
                Tab_calculs$PI_1_Bromopsis_erecta,
                Tab_calculs$PI_1_Onobrychis_viciifolia,
                Tab_calculs$PI_1_Plantago_lanceolata,
                Tab_calculs$PI_1_Festuca_cinerea)
indicePI_2 <- c(Tab_calculs$PI_2_Anthyllis_vulneraria,
                Tab_calculs$PI_2_Bromopsis_erecta,
                Tab_calculs$PI_2_Onobrychis_viciifolia,
                Tab_calculs$PI_2_Plantago_lanceolata,
                Tab_calculs$PI_2_Festuca_cinerea)
indiceEI_1 <- c(Tab_calculs$EI_1_Anthyllis_vulneraria,
                Tab_calculs$EI_1_Bromopsis_erecta,
                Tab_calculs$EI_1_Onobrychis_viciifolia,
                Tab_calculs$EI_1_Plantago_lanceolata,
                Tab_calculs$EI_1_Festuca_cinerea)
indiceEI_2 <- c(Tab_calculs$EI_2_Anthyllis_vulneraria,
                Tab_calculs$EI_2_Bromopsis_erecta,
                Tab_calculs$EI_2_Onobrychis_viciifolia,
                Tab_calculs$EI_2_Plantago_lanceolata,
                Tab_calculs$EI_2_Festuca_cinerea)

Facteur_indices <- factor(rep(c("Anthyllis
vulneraria","Bromopsis
erecta","Onobrychis
viciifolia","Plantago
lanceolata","Festuca
cinerea"),each=10),levels=c("Bromopsis
erecta","Anthyllis
vulneraria" ,"Festuca
cinerea","Onobrychis
viciifolia","Plantago
lanceolata"))

(Tab_all_indices <- data.frame(indicePI_1,indicePI_2,indiceEI_1,indiceEI_2,Facteur_indices))
```

```
##    indicePI_1 indicePI_2  indiceEI_1 indiceEI_2        Facteur_indices
## 1   1.8933333  0.6375000 -0.54545455  0.4428571  Anthyllis\nvulneraria
## 2   1.9680000  0.4428571  1.68000000  1.0916667  Anthyllis\nvulneraria
## 3   1.9200000 -0.6202532 -0.54545455  1.0916667  Anthyllis\nvulneraria
## 4   1.9600000 -0.1525424  1.36000000  1.6885714  Anthyllis\nvulneraria
## 5   1.3600000 -0.8404255 -0.54545455 -0.7752809  Anthyllis\nvulneraria
## 6   1.9360000 -0.1525424 -0.54545455  1.0916667  Anthyllis\nvulneraria
## 7   1.8933333  0.4428571 -0.54545455  1.2733333  Anthyllis\nvulneraria
## 8   1.8400000 -0.8404255 -1.00000000 -1.0000000  Anthyllis\nvulneraria
## 9   1.9542857 -0.1525424  0.40000000  1.2733333  Anthyllis\nvulneraria
## 10  1.9680000 -0.4202899  1.36000000 -0.7752809  Anthyllis\nvulneraria
## 11 -0.3808050 -0.9000000 -0.98050682 -0.7777778      Bromopsis\nerecta
## 12 -0.9497992 -0.9622642  1.41888889  0.7777778      Bromopsis\nerecta
## 13  0.6925000 -0.8297872  1.47700000  0.7777778      Bromopsis\nerecta
## 14 -0.8942918 -0.9000000  1.12833333 -0.4285714      Bromopsis\nerecta
## 15 -0.3808050 -0.6250000 -0.08424908 -0.4285714      Bromopsis\nerecta
## 16 -0.8942918 -0.9622642 -1.00000000 -0.6250000      Bromopsis\nerecta
## 17 -0.7635934 -0.9000000  1.30266667  0.1666667      Bromopsis\nerecta
## 18 -0.8942918 -0.9000000 -1.00000000 -1.0000000      Bromopsis\nerecta
## 19 -0.8942918 -0.9719626 -0.38080495 -0.4285714      Bromopsis\nerecta
## 20 -0.8942918 -0.8297872 -0.38080495 -0.6250000      Bromopsis\nerecta
## 21 -0.7933884 -0.7448980  0.83200000  1.2971429 Onobrychis\nviciifolia
## 22 -0.7933884 -0.9115044  0.05333333  0.7700000 Onobrychis\nviciifolia
## 23 -0.9825784 -0.9959184 -0.74137931 -0.6022727 Onobrychis\nviciifolia
## 24 -0.9825784 -0.9115044 -0.47916667 -0.3150685 Onobrychis\nviciifolia
## 25 -0.7933884 -0.9959184 -0.79338843  0.3600000 Onobrychis\nviciifolia
## 26 -0.7933884 -0.9918033 -0.65437788 -0.7448980 Onobrychis\nviciifolia
## 27 -0.7933884 -0.9792531 -0.25149701  0.7700000 Onobrychis\nviciifolia
## 28 -0.7933884  0.3600000 -0.47916667  0.7700000 Onobrychis\nviciifolia
## 29 -0.7933884 -0.9959184 -0.79338843  0.3600000 Onobrychis\nviciifolia
## 30 -0.7933884 -0.7448980  1.16571429  1.1800000 Onobrychis\nviciifolia
## 31 -0.3421053 -1.0000000 -0.91379310  0.4700000   Plantago\nlanceolata
## 32 -1.0000000 -1.0000000 -0.81132075  1.3880000   Plantago\nlanceolata
## 33 -1.0000000 -1.0000000 -0.91379310  0.7760000   Plantago\nlanceolata
## 34 -0.3421053 -1.0000000 -0.91379310  1.2350000   Plantago\nlanceolata
## 35 -0.9137931 -0.9833887 -1.00000000  1.5920000   Plantago\nlanceolata
## 36 -0.9137931 -0.9110320 -0.81132075  1.2350000   Plantago\nlanceolata
## 37 -1.0000000 -1.0000000 -1.00000000 -0.5145631   Plantago\nlanceolata
## 38  0.7400000  0.4700000 -0.09090909  1.8470000   Plantago\nlanceolata
## 39 -1.0000000 -0.9833887 -0.68750000  1.7552000   Plantago\nlanceolata
## 40 -0.9137931 -1.0000000 -0.91379310  1.4900000   Plantago\nlanceolata
## 41  1.1100000  0.3600000  1.64400000  1.5900000       Festuca\ncinerea
## 42 -1.0000000  0.3600000 -0.79729730  1.4533333       Festuca\ncinerea
## 43 -0.6093750 -0.9350649 -0.87341772  1.1800000       Festuca\ncinerea
## 44 -0.9404762 -0.8611111  0.22000000  1.4533333       Festuca\ncinerea
## 45 -1.0000000 -0.9750000 -0.87341772 -0.4230769       Festuca\ncinerea
## 46  0.2200000 -0.9750000  1.40666667  1.5900000       Festuca\ncinerea
## 47 -1.0000000  0.3600000 -0.60937500  1.5900000       Festuca\ncinerea
## 48 -0.6093750  0.3600000 -0.71014493  0.3600000       Festuca\ncinerea
## 49 -0.6093750 -0.9350649  0.22000000  1.3166667       Festuca\ncinerea
## 50 -1.0000000 -1.0000000 -0.87341772  0.9750000       Festuca\ncinerea
```

## Figure S2

The GLMs and post hoc

* Unsown cover


```r
Cover$unsownP <- Cover$unsown/100
unsown21 <-glm(unsownP ~ Compo*Year, family=quasibinomial, data=Cover)
plotresid(unsown21)
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-16-1.png)<!-- -->

```r
summary(unsown21)
```

```
## 
## Call:
## glm(formula = unsownP ~ Compo * Year, family = quasibinomial, 
##     data = Cover)
## 
## Deviance Residuals: 
##     Min       1Q   Median       3Q      Max  
## -0.8746  -0.2828  -0.1225   0.1513   1.0473  
## 
## Coefficients:
##                 Estimate Std. Error t value Pr(>|t|)    
## (Intercept)     -2.58669    0.44066  -5.870 4.86e-08 ***
## Compo1          -0.85559    0.78422  -1.091   0.2777    
## Compo2          -0.04702    0.62960  -0.075   0.9406    
## CompoT           0.28520    0.58906   0.484   0.6292    
## Year2020        -0.04702    0.62960  -0.075   0.9406    
## Year2021        -0.09604    0.63655  -0.151   0.8804    
## Compo1:Year2020  1.13847    0.98811   1.152   0.2518    
## Compo2:Year2020  0.79735    0.84199   0.947   0.3458    
## CompoT:Year2020  1.04157    0.79033   1.318   0.1903    
## Compo1:Year2021  1.09597    0.99888   1.097   0.2750    
## Compo2:Year2021  0.23178    0.88737   0.261   0.7944    
## CompoT:Year2021  1.78288    0.78327   2.276   0.0248 *  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## (Dispersion parameter for quasibinomial family taken to be 0.1264099)
## 
##     Null deviance: 20.537  on 119  degrees of freedom
## Residual deviance: 13.507  on 108  degrees of freedom
## AIC: NA
## 
## Number of Fisher Scoring iterations: 6
```

```r
anova(unsown21,test="F")
```

```
## Analysis of Deviance Table
## 
## Model: quasibinomial, link: logit
## 
## Response: unsownP
## 
## Terms added sequentially (first to last)
## 
## 
##            Df Deviance Resid. Df Resid. Dev       F    Pr(>F)    
## NULL                         119     20.537                      
## Compo       3   4.3307       116     16.206 11.4198 1.455e-06 ***
## Year        2   1.5132       114     14.693  5.9853  0.003426 ** 
## Compo:Year  6   1.1858       108     13.507  1.5635  0.164773    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

```r
emmeans(unsown21, pairwise ~ Compo*Year, adjust="tukey",alpha = .05) 
```

```
## $emmeans
##  Compo Year emmean    SE  df asymp.LCL asymp.UCL
##  12    2019 -2.587 0.441 Inf     -3.45    -1.723
##  1     2019 -3.442 0.649 Inf     -4.71    -2.171
##  2     2019 -2.634 0.450 Inf     -3.52    -1.752
##  T     2019 -2.301 0.391 Inf     -3.07    -1.535
##  12    2020 -2.634 0.450 Inf     -3.52    -1.752
##  1     2020 -2.351 0.399 Inf     -3.13    -1.569
##  2     2020 -1.883 0.332 Inf     -2.53    -1.232
##  T     2020 -1.307 0.275 Inf     -1.85    -0.769
##  12    2021 -2.683 0.459 Inf     -3.58    -1.782
##  1     2021 -2.442 0.414 Inf     -3.25    -1.630
##  2     2021 -2.498 0.424 Inf     -3.33    -1.666
##  T     2021 -0.615 0.236 Inf     -1.08    -0.153
## 
## Results are given on the logit (not the response) scale. 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast                  estimate    SE  df z.ratio p.value
##  12 Year2019 - 1 Year2019    0.8556 0.784 Inf   1.091  0.9951
##  12 Year2019 - 2 Year2019    0.0470 0.630 Inf   0.075  1.0000
##  12 Year2019 - T Year2019   -0.2852 0.589 Inf  -0.484  1.0000
##  12 Year2019 - 12 Year2020   0.0470 0.630 Inf   0.075  1.0000
##  12 Year2019 - 1 Year2020   -0.2359 0.594 Inf  -0.397  1.0000
##  12 Year2019 - 2 Year2020   -0.7033 0.552 Inf  -1.275  0.9823
##  12 Year2019 - T Year2020   -1.2798 0.519 Inf  -2.465  0.3624
##  12 Year2019 - 12 Year2021   0.0960 0.637 Inf   0.151  1.0000
##  12 Year2019 - 1 Year2021   -0.1443 0.605 Inf  -0.239  1.0000
##  12 Year2019 - 2 Year2021   -0.0887 0.612 Inf  -0.145  1.0000
##  12 Year2019 - T Year2021   -1.9720 0.500 Inf  -3.947  0.0045
##  1 Year2019 - 2 Year2019    -0.8086 0.789 Inf  -1.024  0.9972
##  1 Year2019 - T Year2019    -1.1408 0.757 Inf  -1.506  0.9393
##  1 Year2019 - 12 Year2020   -0.8086 0.789 Inf  -1.024  0.9972
##  1 Year2019 - 1 Year2020    -1.0914 0.762 Inf  -1.433  0.9572
##  1 Year2019 - 2 Year2020    -1.5589 0.729 Inf  -2.139  0.5944
##  1 Year2019 - T Year2020    -2.1353 0.704 Inf  -3.031  0.0996
##  1 Year2019 - 12 Year2021   -0.7595 0.795 Inf  -0.956  0.9985
##  1 Year2019 - 1 Year2021    -0.9999 0.770 Inf  -1.299  0.9795
##  1 Year2019 - 2 Year2021    -0.9443 0.775 Inf  -1.218  0.9877
##  1 Year2019 - T Year2021    -2.8276 0.690 Inf  -4.097  0.0025
##  2 Year2019 - T Year2019    -0.3322 0.596 Inf  -0.558  1.0000
##  2 Year2019 - 12 Year2020    0.0000 0.636 Inf   0.000  1.0000
##  2 Year2019 - 1 Year2020    -0.2829 0.601 Inf  -0.471  1.0000
##  2 Year2019 - 2 Year2020    -0.7503 0.559 Inf  -1.342  0.9736
##  2 Year2019 - T Year2020    -1.3268 0.527 Inf  -2.518  0.3283
##  2 Year2019 - 12 Year2021    0.0490 0.643 Inf   0.076  1.0000
##  2 Year2019 - 1 Year2021    -0.1914 0.612 Inf  -0.313  1.0000
##  2 Year2019 - 2 Year2021    -0.1357 0.618 Inf  -0.220  1.0000
##  2 Year2019 - T Year2021    -2.0191 0.508 Inf  -3.977  0.0040
##  T Year2019 - 12 Year2020    0.3322 0.596 Inf   0.558  1.0000
##  T Year2019 - 1 Year2020     0.0493 0.559 Inf   0.088  1.0000
##  T Year2019 - 2 Year2020    -0.4181 0.513 Inf  -0.815  0.9997
##  T Year2019 - T Year2020    -0.9945 0.478 Inf  -2.082  0.6360
##  T Year2019 - 12 Year2021    0.3812 0.603 Inf   0.632  1.0000
##  T Year2019 - 1 Year2021     0.1409 0.570 Inf   0.247  1.0000
##  T Year2019 - 2 Year2021     0.1965 0.577 Inf   0.341  1.0000
##  T Year2019 - T Year2021    -1.6868 0.456 Inf  -3.696  0.0118
##  12 Year2020 - 1 Year2020   -0.2829 0.601 Inf  -0.471  1.0000
##  12 Year2020 - 2 Year2020   -0.7503 0.559 Inf  -1.342  0.9736
##  12 Year2020 - T Year2020   -1.3268 0.527 Inf  -2.518  0.3283
##  12 Year2020 - 12 Year2021   0.0490 0.643 Inf   0.076  1.0000
##  12 Year2020 - 1 Year2021   -0.1914 0.612 Inf  -0.313  1.0000
##  12 Year2020 - 2 Year2021   -0.1357 0.618 Inf  -0.220  1.0000
##  12 Year2020 - T Year2021   -2.0191 0.508 Inf  -3.977  0.0040
##  1 Year2020 - 2 Year2020    -0.4674 0.519 Inf  -0.900  0.9991
##  1 Year2020 - T Year2020    -1.0439 0.484 Inf  -2.155  0.5824
##  1 Year2020 - 12 Year2021    0.3319 0.608 Inf   0.546  1.0000
##  1 Year2020 - 1 Year2021     0.0915 0.575 Inf   0.159  1.0000
##  1 Year2020 - 2 Year2021     0.1472 0.582 Inf   0.253  1.0000
##  1 Year2020 - T Year2021    -1.7362 0.463 Inf  -3.748  0.0098
##  2 Year2020 - T Year2020    -0.5765 0.431 Inf  -1.338  0.9743
##  2 Year2020 - 12 Year2021    0.7993 0.567 Inf   1.410  0.9619
##  2 Year2020 - 1 Year2021     0.5590 0.531 Inf   1.052  0.9964
##  2 Year2020 - 2 Year2021     0.6146 0.539 Inf   1.141  0.9929
##  2 Year2020 - T Year2021    -1.2687 0.407 Inf  -3.116  0.0786
##  T Year2020 - 12 Year2021    1.3758 0.535 Inf   2.571  0.2964
##  T Year2020 - 1 Year2021     1.1354 0.497 Inf   2.284  0.4882
##  T Year2020 - 2 Year2021     1.1910 0.505 Inf   2.357  0.4360
##  T Year2020 - T Year2021    -0.6923 0.362 Inf  -1.913  0.7511
##  12 Year2021 - 1 Year2021   -0.2404 0.619 Inf  -0.389  1.0000
##  12 Year2021 - 2 Year2021   -0.1848 0.625 Inf  -0.295  1.0000
##  12 Year2021 - T Year2021   -2.0681 0.516 Inf  -4.006  0.0036
##  1 Year2021 - 2 Year2021     0.0556 0.593 Inf   0.094  1.0000
##  1 Year2021 - T Year2021    -1.8277 0.477 Inf  -3.834  0.0070
##  2 Year2021 - T Year2021    -1.8833 0.485 Inf  -3.881  0.0059
## 
## Results are given on the log odds ratio (not the response) scale. 
## P value adjustment: tukey method for comparing a family of 12 estimates
```

```r
leastsquareCE20 = emmeans(unsown21, pairwise ~ Compo*Year, adjust="tukey")  
cld(leastsquareCE20)
```

```
##  Compo Year emmean    SE  df asymp.LCL asymp.UCL .group
##  1     2019 -3.442 0.649 Inf     -4.71    -2.171  1    
##  12    2021 -2.683 0.459 Inf     -3.58    -1.782  1    
##  2     2019 -2.634 0.450 Inf     -3.52    -1.752  1    
##  12    2020 -2.634 0.450 Inf     -3.52    -1.752  1    
##  12    2019 -2.587 0.441 Inf     -3.45    -1.723  1    
##  2     2021 -2.498 0.424 Inf     -3.33    -1.666  1    
##  1     2021 -2.442 0.414 Inf     -3.25    -1.630  1    
##  1     2020 -2.351 0.399 Inf     -3.13    -1.569  1    
##  T     2019 -2.301 0.391 Inf     -3.07    -1.535  1    
##  2     2020 -1.883 0.332 Inf     -2.53    -1.232  12   
##  T     2020 -1.307 0.275 Inf     -1.85    -0.769  12   
##  T     2021 -0.615 0.236 Inf     -1.08    -0.153   2   
## 
## Results are given on the logit (not the response) scale. 
## Confidence level used: 0.95 
## Results are given on the log odds ratio (not the response) scale. 
## P value adjustment: tukey method for comparing a family of 12 estimates 
## significance level used: alpha = 0.05 
## NOTE: If two or more means share the same grouping symbol,
##       then we cannot show them to be different.
##       But we also did not show them to be the same.
```

* Total species cover


```r
Cover$Total_cover_p <- Cover$Total_cover/100
tot21 <-glm(Total_cover_p ~ Compo*Year, family=gaussian, data=Cover)
plotresid(tot21)
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-17-1.png)<!-- -->

```r
summary(tot21)
```

```
## 
## Call:
## glm(formula = Total_cover_p ~ Compo * Year, family = gaussian, 
##     data = Cover)
## 
## Deviance Residuals: 
##      Min        1Q    Median        3Q       Max  
## -0.27000  -0.07225  -0.01150   0.07625   0.35100  
## 
## Coefficients:
##                 Estimate Std. Error t value Pr(>|t|)    
## (Intercept)      0.17300    0.03954   4.376 2.80e-05 ***
## Compo1          -0.09900    0.05591  -1.771  0.07946 .  
## Compo2          -0.01490    0.05591  -0.266  0.79038    
## CompoT          -0.05070    0.05591  -0.907  0.36656    
## Year2020         0.36700    0.05591   6.564 1.88e-09 ***
## Year2021         0.36200    0.05591   6.474 2.89e-09 ***
## Compo1:Year2020  0.12900    0.07908   1.631  0.10573    
## Compo2:Year2020 -0.05510    0.07908  -0.697  0.48742    
## CompoT:Year2020 -0.24030    0.07908  -3.039  0.00298 ** 
## Compo1:Year2021  0.13600    0.07908   1.720  0.08832 .  
## Compo2:Year2021  0.02490    0.07908   0.315  0.75345    
## CompoT:Year2021 -0.09430    0.07908  -1.193  0.23566    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## (Dispersion parameter for gaussian family taken to be 0.01563218)
## 
##     Null deviance: 5.9396  on 119  degrees of freedom
## Residual deviance: 1.6883  on 108  degrees of freedom
## AIC: -145.11
## 
## Number of Fisher Scoring iterations: 2
```

```r
anova(tot21,test="F") 
```

```
## Analysis of Deviance Table
## 
## Model: gaussian, link: identity
## 
## Response: Total_cover_p
## 
## Terms added sequentially (first to last)
## 
## 
##            Df Deviance Resid. Df Resid. Dev        F    Pr(>F)    
## NULL                         119     5.9396                       
## Compo       3   0.5181       116     5.4215  11.0471 2.203e-06 ***
## Year        2   3.3613       114     2.0602 107.5118 < 2.2e-16 ***
## Compo:Year  6   0.3720       108     1.6883   3.9659  0.001266 ** 
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

```r
emmeans(tot21, pairwise ~ Compo*Year, adjust="tukey",alpha = .05)
```

```
## $emmeans
##  Compo Year emmean     SE  df lower.CL upper.CL
##  12    2019  0.173 0.0395 108  0.09463    0.251
##  1     2019  0.074 0.0395 108 -0.00437    0.152
##  2     2019  0.158 0.0395 108  0.07973    0.236
##  T     2019  0.122 0.0395 108  0.04393    0.201
##  12    2020  0.540 0.0395 108  0.46163    0.618
##  1     2020  0.570 0.0395 108  0.49163    0.648
##  2     2020  0.470 0.0395 108  0.39163    0.548
##  T     2020  0.249 0.0395 108  0.17063    0.327
##  12    2021  0.535 0.0395 108  0.45663    0.613
##  1     2021  0.572 0.0395 108  0.49363    0.650
##  2     2021  0.545 0.0395 108  0.46663    0.623
##  T     2021  0.390 0.0395 108  0.31163    0.468
## 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast                  estimate     SE  df t.ratio p.value
##  12 Year2019 - 1 Year2019    0.0990 0.0559 108   1.771  0.8300
##  12 Year2019 - 2 Year2019    0.0149 0.0559 108   0.266  1.0000
##  12 Year2019 - T Year2019    0.0507 0.0559 108   0.907  0.9989
##  12 Year2019 - 12 Year2020  -0.3670 0.0559 108  -6.564  <.0001
##  12 Year2019 - 1 Year2020   -0.3970 0.0559 108  -7.100  <.0001
##  12 Year2019 - 2 Year2020   -0.2970 0.0559 108  -5.312  <.0001
##  12 Year2019 - T Year2020   -0.0760 0.0559 108  -1.359  0.9686
##  12 Year2019 - 12 Year2021  -0.3620 0.0559 108  -6.474  <.0001
##  12 Year2019 - 1 Year2021   -0.3990 0.0559 108  -7.136  <.0001
##  12 Year2019 - 2 Year2021   -0.3720 0.0559 108  -6.653  <.0001
##  12 Year2019 - T Year2021   -0.2170 0.0559 108  -3.881  0.0094
##  1 Year2019 - 2 Year2019    -0.0841 0.0559 108  -1.504  0.9364
##  1 Year2019 - T Year2019    -0.0483 0.0559 108  -0.864  0.9993
##  1 Year2019 - 12 Year2020   -0.4660 0.0559 108  -8.334  <.0001
##  1 Year2019 - 1 Year2020    -0.4960 0.0559 108  -8.871  <.0001
##  1 Year2019 - 2 Year2020    -0.3960 0.0559 108  -7.082  <.0001
##  1 Year2019 - T Year2020    -0.1750 0.0559 108  -3.130  0.0887
##  1 Year2019 - 12 Year2021   -0.4610 0.0559 108  -8.245  <.0001
##  1 Year2019 - 1 Year2021    -0.4980 0.0559 108  -8.906  <.0001
##  1 Year2019 - 2 Year2021    -0.4710 0.0559 108  -8.424  <.0001
##  1 Year2019 - T Year2021    -0.3160 0.0559 108  -5.651  <.0001
##  2 Year2019 - T Year2019     0.0358 0.0559 108   0.640  1.0000
##  2 Year2019 - 12 Year2020   -0.3819 0.0559 108  -6.830  <.0001
##  2 Year2019 - 1 Year2020    -0.4119 0.0559 108  -7.367  <.0001
##  2 Year2019 - 2 Year2020    -0.3119 0.0559 108  -5.578  <.0001
##  2 Year2019 - T Year2020    -0.0909 0.0559 108  -1.626  0.8958
##  2 Year2019 - 12 Year2021   -0.3769 0.0559 108  -6.741  <.0001
##  2 Year2019 - 1 Year2021    -0.4139 0.0559 108  -7.402  <.0001
##  2 Year2019 - 2 Year2021    -0.3869 0.0559 108  -6.919  <.0001
##  2 Year2019 - T Year2021    -0.2319 0.0559 108  -4.147  0.0037
##  T Year2019 - 12 Year2020   -0.4177 0.0559 108  -7.470  <.0001
##  T Year2019 - 1 Year2020    -0.4477 0.0559 108  -8.007  <.0001
##  T Year2019 - 2 Year2020    -0.3477 0.0559 108  -6.218  <.0001
##  T Year2019 - T Year2020    -0.1267 0.0559 108  -2.266  0.5068
##  T Year2019 - 12 Year2021   -0.4127 0.0559 108  -7.381  <.0001
##  T Year2019 - 1 Year2021    -0.4497 0.0559 108  -8.043  <.0001
##  T Year2019 - 2 Year2021    -0.4227 0.0559 108  -7.560  <.0001
##  T Year2019 - T Year2021    -0.2677 0.0559 108  -4.788  0.0003
##  12 Year2020 - 1 Year2020   -0.0300 0.0559 108  -0.537  1.0000
##  12 Year2020 - 2 Year2020    0.0700 0.0559 108   1.252  0.9831
##  12 Year2020 - T Year2020    0.2910 0.0559 108   5.204  0.0001
##  12 Year2020 - 12 Year2021   0.0050 0.0559 108   0.089  1.0000
##  12 Year2020 - 1 Year2021   -0.0320 0.0559 108  -0.572  1.0000
##  12 Year2020 - 2 Year2021   -0.0050 0.0559 108  -0.089  1.0000
##  12 Year2020 - T Year2021    0.1500 0.0559 108   2.683  0.2493
##  1 Year2020 - 2 Year2020     0.1000 0.0559 108   1.788  0.8206
##  1 Year2020 - T Year2020     0.3210 0.0559 108   5.741  <.0001
##  1 Year2020 - 12 Year2021    0.0350 0.0559 108   0.626  1.0000
##  1 Year2020 - 1 Year2021    -0.0020 0.0559 108  -0.036  1.0000
##  1 Year2020 - 2 Year2021     0.0250 0.0559 108   0.447  1.0000
##  1 Year2020 - T Year2021     0.1800 0.0559 108   3.219  0.0700
##  2 Year2020 - T Year2020     0.2210 0.0559 108   3.952  0.0073
##  2 Year2020 - 12 Year2021   -0.0650 0.0559 108  -1.162  0.9907
##  2 Year2020 - 1 Year2021    -0.1020 0.0559 108  -1.824  0.8011
##  2 Year2020 - 2 Year2021    -0.0750 0.0559 108  -1.341  0.9715
##  2 Year2020 - T Year2021     0.0800 0.0559 108   1.431  0.9547
##  T Year2020 - 12 Year2021   -0.2860 0.0559 108  -5.115  0.0001
##  T Year2020 - 1 Year2021    -0.3230 0.0559 108  -5.777  <.0001
##  T Year2020 - 2 Year2021    -0.2960 0.0559 108  -5.294  <.0001
##  T Year2020 - T Year2021    -0.1410 0.0559 108  -2.522  0.3382
##  12 Year2021 - 1 Year2021   -0.0370 0.0559 108  -0.662  0.9999
##  12 Year2021 - 2 Year2021   -0.0100 0.0559 108  -0.179  1.0000
##  12 Year2021 - T Year2021    0.1450 0.0559 108   2.593  0.2967
##  1 Year2021 - 2 Year2021     0.0270 0.0559 108   0.483  1.0000
##  1 Year2021 - T Year2021     0.1820 0.0559 108   3.255  0.0635
##  2 Year2021 - T Year2021     0.1550 0.0559 108   2.772  0.2071
## 
## P value adjustment: tukey method for comparing a family of 12 estimates
```

```r
leastsquareCE21 = emmeans(tot21, pairwise ~ Compo*Year, adjust="tukey")  
cld(leastsquareCE21)
```

```
##  Compo Year emmean     SE  df lower.CL upper.CL .group
##  1     2019  0.074 0.0395 108 -0.00437    0.152  1    
##  T     2019  0.122 0.0395 108  0.04393    0.201  1    
##  2     2019  0.158 0.0395 108  0.07973    0.236  1    
##  12    2019  0.173 0.0395 108  0.09463    0.251  1    
##  T     2020  0.249 0.0395 108  0.17063    0.327  12   
##  T     2021  0.390 0.0395 108  0.31163    0.468   23  
##  2     2020  0.470 0.0395 108  0.39163    0.548    3  
##  12    2021  0.535 0.0395 108  0.45663    0.613    3  
##  12    2020  0.540 0.0395 108  0.46163    0.618    3  
##  2     2021  0.545 0.0395 108  0.46663    0.623    3  
##  1     2020  0.570 0.0395 108  0.49163    0.648    3  
##  1     2021  0.572 0.0395 108  0.49363    0.650    3  
## 
## Confidence level used: 0.95 
## P value adjustment: tukey method for comparing a family of 12 estimates 
## significance level used: alpha = 0.05 
## NOTE: If two or more means share the same grouping symbol,
##       then we cannot show them to be different.
##       But we also did not show them to be the same.
```

The plot


```r
par(mfrow=c(1,2))

coulnew23 <- c("#114958","#114958","#114958",  "#7AC9A3","#7AC9A3",
               "#7AC9A3", "#F59036", "#F59036","#F59036","#96A393", 
               "#96A393", "#96A393") 

# Total :
barres.plot.beside(VARI = Cover$Total_cover, 
                   FAC1 = factor(Cover$Compo, levels=c("12","2", "1","T")), 
                   FAC2 = factor(Cover$Year), POSI = "bottom", col=coulnew23,  
                   density=c(0,30,200,0,30, 200,0,30,200,0,30,200), 
                   angle=c(45,45,0,45,45,0,45, 45,0),
                   ylab="Total species cover in 2021 (%)", 
                   border=coulnew23,  font.lab=2, cex.lab=1.5, 
                   labels.x = c("D + S" ,"D1st", "S1st", "Control"), 
                   cex.x = 1.2, font.x=2, below.x=0.3, ylim = c(0,80),
                   lettres = c("a","c","c","a","c","c",
                               "a","c","c", "a", "ab", "bc"))
legend("topleft", "Treatment: df=3; ;F=11.047, P<0.001
Year: df=2, F=107.51; P<0.001
Treatment × Year: df=6, F= 3.9659, P<0.01", bty = "n", cex = 1, 
text.font = 1)


# unsown :
barres.plot.beside(VARI = Cover$unsown, 
                   FAC1 = factor(Cover$Compo, levels=c("12","2", "1","T")), 
                   FAC2 = factor(Cover$Year), POSI = "bottom", col=coulnew23,  
                   density=c(0,30,200,0,30, 200,0,30,200,0,30,200), 
                   angle=c(45,45,0,45,45,0,45, 45,0),
                   ylab="Unsown species cover in 2021(%)", border=coulnew23,  
                   font.lab=2, cex.lab=1.5, 
                   labels.x = c("D + S" ,"D1st", "S1st", "Control"), cex.x = 1.2,
                   font.x=2, below.x=0.3, ylim = c(0,50), 
                   lettres = c("a","a","a","a","ab","a","a","a",
                               "a", "a", "ab", "b"))
legend("topleft", "Treatment: df=3; ;F=11.420, P<0.001
Year: df=2, F=5.9853; P<0.01, 
Treatment × Year: df=6, F= 1.5635, P>0.05
GLM quasibinomial", bty = "n", cex = 1, text.font = 1)
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-18-1.png)<!-- -->

```r
par(mfrow=c(1,1))
```

## Figure S3

The stats

* PI esp GLM


```r
indicePI_2_t <- (indicePI_2+1.0001)/3.0001
# transformation to be between 0 and 1
plot(indicePI_2_t, indicePI_2) #check that it does not change the distribution structure
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-19-1.png)<!-- -->

```r
Tab_indices_esp <- data.frame(indicePI_2,indicePI_2_t ,indiceEI_2, Facteur_indices )

glm_esp_PI <- glm(indicePI_2_t ~ Facteur_indices , family=Gamma(link="log"), 
                  data=Tab_indices_esp)
plotresid(glm_esp_PI)
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-19-2.png)<!-- -->

```r
summary(glm_esp_PI)
```

```
## 
## Call:
## glm(formula = indicePI_2_t ~ Facteur_indices, family = Gamma(link = "log"), 
##     data = Tab_indices_esp)
## 
## Deviance Residuals: 
##      Min        1Q    Median        3Q       Max  
## -13.3286   -2.4213   -0.9807    0.1591    2.6968  
## 
## Coefficients:
##                                       Estimate Std. Error t value Pr(>|t|)    
## (Intercept)                            -3.2024     0.3890  -8.232 1.59e-10 ***
## Facteur_indicesAnthyllis\nvulneraria    1.9229     0.5502   3.495  0.00108 ** 
## Facteur_indicesFestuca\ncinerea         1.5521     0.5502   2.821  0.00710 ** 
## Facteur_indicesOnobrychis\nviciifolia   0.5381     0.5502   0.978  0.33329    
## Facteur_indicesPlantago\nlanceolata    82.7186     0.5502 150.354  < 2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## (Dispersion parameter for Gamma family taken to be 1.513365)
## 
##     Null deviance:  201.14  on 49  degrees of freedom
## Residual deviance: 1802.12  on 45  degrees of freedom
## AIC: 23.925
## 
## Number of Fisher Scoring iterations: 25
```

```r
anova(glm_esp_PI,test="F")
```

```
## Analysis of Deviance Table
## 
## Model: Gamma, link: log
## 
## Response: indicePI_2_t
## 
## Terms added sequentially (first to last)
## 
## 
##                 Df Deviance Resid. Df Resid. Dev  F Pr(>F)
## NULL                               49     201.14          
## Facteur_indices  4        0        45    1802.12  0      1
```

* EI esp GLM


```r
indiceEI_2_t <- (indiceEI_2+1.0001)/3.0001
# transformation to be between 0 and 1
plot(indiceEI_2_t, indiceEI_2) 
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-20-1.png)<!-- -->

```r
#check that it does not change the distribution structure
Tab_indices_esp <- data.frame(indicePI_2,indicePI_2_t ,
                              indiceEI_2, indiceEI_2_t, Facteur_indices )
glm_esp_EI <- glm(indiceEI_2_t ~ Facteur_indices , family=Gamma(link="log"),
                  data=Tab_indices_esp)
plotresid(glm_esp_EI)
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-20-2.png)<!-- -->

```r
summary(glm_esp_EI)
```

```
## 
## Call:
## glm(formula = indiceEI_2_t ~ Facteur_indices, family = Gamma(link = "log"), 
##     data = Tab_indices_esp)
## 
## Deviance Residuals: 
##     Min       1Q   Median       3Q      Max  
## -4.1575  -0.3935   0.0727   0.2667   1.0239  
## 
## Coefficients:
##                                       Estimate Std. Error t value Pr(>|t|)    
## (Intercept)                            -1.3984     0.1799  -7.775 7.34e-10 ***
## Facteur_indicesAnthyllis\nvulneraria    0.7318     0.2544   2.877 0.006119 ** 
## Facteur_indicesFestuca\ncinerea         1.0458     0.2544   4.111 0.000165 ***
## Facteur_indicesOnobrychis\nviciifolia   0.6252     0.2544   2.458 0.017894 *  
## Facteur_indicesPlantago\nlanceolata     1.0547     0.2544   4.146 0.000147 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## (Dispersion parameter for Gamma family taken to be 0.3235168)
## 
##     Null deviance: 56.586  on 49  degrees of freedom
## Residual deviance: 49.989  on 45  degrees of freedom
## AIC: 41.306
## 
## Number of Fisher Scoring iterations: 7
```

```r
anova(glm_esp_EI,test="F")
```

```
## Analysis of Deviance Table
## 
## Model: Gamma, link: log
## 
## Response: indiceEI_2_t
## 
## Terms added sequentially (first to last)
## 
## 
##                 Df Deviance Resid. Df Resid. Dev      F   Pr(>F)   
## NULL                               49     56.586                   
## Facteur_indices  4    6.596        45     49.989 5.0971 0.001788 **
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

```r
emmeans(glm_esp_EI, pairwise ~ Facteur_indices, adjust="tukey",alpha = .05)
```

```
## $emmeans
##   Facteur_indices       emmean   SE df lower.CL upper.CL
##  Bromopsis\nerecta      -1.398 0.18 45   -1.761 -1.03617
##  Anthyllis\nvulneraria  -0.667 0.18 45   -1.029 -0.30437
##  Festuca\ncinerea       -0.353 0.18 45   -0.715  0.00966
##  Onobrychis\nviciifolia -0.773 0.18 45   -1.136 -0.41097
##  Plantago\nlanceolata   -0.344 0.18 45   -0.706  0.01855
## 
## Results are given on the log (not the response) scale. 
## Confidence level used: 0.95 
## 
## $contrasts
##    contrast                                     estimate    SE df t.ratio
##  Bromopsis\nerecta - Anthyllis\nvulneraria      -0.73180 0.254 45  -2.877
##  Bromopsis\nerecta - Festuca\ncinerea           -1.04583 0.254 45  -4.111
##  Bromopsis\nerecta - Onobrychis\nviciifolia     -0.62520 0.254 45  -2.458
##  Bromopsis\nerecta - Plantago\nlanceolata       -1.05472 0.254 45  -4.146
##  Anthyllis\nvulneraria - Festuca\ncinerea       -0.31402 0.254 45  -1.235
##  Anthyllis\nvulneraria - Onobrychis\nviciifolia  0.10661 0.254 45   0.419
##  Anthyllis\nvulneraria - Plantago\nlanceolata   -0.32292 0.254 45  -1.269
##  Festuca\ncinerea - Onobrychis\nviciifolia       0.42063 0.254 45   1.654
##  Festuca\ncinerea - Plantago\nlanceolata        -0.00889 0.254 45  -0.035
##  Onobrychis\nviciifolia - Plantago\nlanceolata  -0.42953 0.254 45  -1.689
##  p.value
##   0.0459
##   0.0015
##   0.1189
##   0.0013
##   0.7315
##   0.9933
##   0.7109
##   0.4722
##   1.0000
##   0.4511
## 
## Results are given on the log (not the response) scale. 
## P value adjustment: tukey method for comparing a family of 5 estimates
```

```r
leastsquareCEespEI <- emmeans(glm_esp_EI, pairwise ~ Facteur_indices, 
                              adjust="tukey")  
cld(leastsquareCEespEI)
```

```
##   Facteur_indices       emmean   SE df lower.CL upper.CL .group
##  Bromopsis\nerecta      -1.398 0.18 45   -1.761 -1.03617  1    
##  Onobrychis\nviciifolia -0.773 0.18 45   -1.136 -0.41097  12   
##  Anthyllis\nvulneraria  -0.667 0.18 45   -1.029 -0.30437   2   
##  Festuca\ncinerea       -0.353 0.18 45   -0.715  0.00966   2   
##  Plantago\nlanceolata   -0.344 0.18 45   -0.706  0.01855   2   
## 
## Results are given on the log (not the response) scale. 
## Confidence level used: 0.95 
## P value adjustment: tukey method for comparing a family of 5 estimates 
## significance level used: alpha = 0.05 
## NOTE: If two or more means share the same grouping symbol,
##       then we cannot show them to be different.
##       But we also did not show them to be the same.
```

The figures


```r
coul_IP<- c("#2738B3","#128510","#7E8CF7","#8CFF8A","#2FFAF3")
par(mfrow=c(1,2))
barres.plot(variable = indicePI_2,Facteur = Facteur_indices,ylim = c(-1.6,2), 
            col = coul_IP, border= coul_IP, ylab="PI (covers) n = 2 years", 
            las=1, ecart = "IC", lettres=c("a","a","a","a","a"))

barres.plot(variable = indiceEI_2,Facteur = Facteur_indices,ylim = c(-1.6,2), 
            col = coul_IP, border= coul_IP,  ylab="EI (covers) n = 2 years", 
            las=1, ecart = "IC", lettres=c("a","b","b","ab","b"))
```

![](Script_Article_Durance_EP_files/figure-html/unnamed-chunk-21-1.png)<!-- -->

```r
par(mfrow=c(1,1))
```



